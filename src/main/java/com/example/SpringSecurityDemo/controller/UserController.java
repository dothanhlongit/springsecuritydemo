package com.example.SpringSecurityDemo.controller;

import com.example.SpringSecurityDemo.service.UserService;
import com.example.SpringSecurityDemo.ui.model.dto.UserDTO;
import com.example.SpringSecurityDemo.ui.model.request.UserDetailRequestModel;
import com.example.SpringSecurityDemo.ui.model.response.UserRest;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    UserService userService;

    @PostMapping
    UserRest createUser(@RequestBody UserDetailRequestModel userDetailRequestModel) {
        UserRest returnValue = new UserRest();

        UserDTO userDTO = new UserDTO();
        BeanUtils.copyProperties(userDetailRequestModel, userDTO);

        UserDTO createdUser = userService.createUser(userDTO);
        BeanUtils.copyProperties(createdUser, returnValue);

        return returnValue;
    }

    @GetMapping("/{id}")
    UserRest getUser(@PathVariable Long id) {
        UserRest returnValue = new UserRest();
        UserDTO getUser = userService.getUser(id);

        UserDTO userDTO = new UserDTO();
        BeanUtils.copyProperties(getUser, userDTO);
        BeanUtils.copyProperties(userDTO, returnValue);

        return returnValue;
    }

    @GetMapping
    String getMapping() {
        return "user api";
    }
}
