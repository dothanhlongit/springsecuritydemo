package com.example.SpringSecurityDemo.service;

import com.example.SpringSecurityDemo.ui.model.dto.UserDTO;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserService extends UserDetailsService {

    UserDTO createUser(UserDTO userDTO);

    UserDTO getUser(String email);

    UserDTO getUser(Long id);
}
